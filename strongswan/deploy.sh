#!/bin/bash

#set -eu -o pipefail # fail on error , debug all lines

#sudo -n true
#test $? -eq 0 || exit 1 "you should have sudo priveledge to run this script"

#LEFT_SUBNET=$(hostname -I)
##cloud cert
#LEFT_CERT_FILE=ca.crt
##deploying host cert
#RIGTH_CERT_FILE=host.crt

source values.sh

function render_template() {
  eval "echo \"$(cat $1)\""
}

function generate_ipsec_conf {
  render_template ipsec.conf.tmpl > /etc/ipsec.conf
}

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

if dpkg --list strongswan 
then
 echo StrongSwan already installed.
else 
 echo StrongSwan not found installing...
 apt update
 apt install strongswan=5.8.2-1ubuntu3.1
fi

generate_ipsec_conf

#cp $LEFT_CERT_FILE $RIGTH_CERT_FILE /etc/ipsec.d/certs
#cp $LEFT_KEY_FILE /etc/ipsec.d/private

#ssh-keygen -t rsa -b 2048 -m PEM -f host -N ""
openssl req -x509  -days 3650 -newkey rsa:2048 -keyout host.pem -out host.crt -nodes -batch
mv host.pem /etc/ipsec.d/private/host.pem
mv host.crt /etc/ipsec.d/certs
echo "Generated certificate:"
cat /etc/ipsec.d/certs/host.crt
echo "This key need to be added for cloud Mikrotik"
echo ": RSA host.pem">>/etc/ipsec.secrets
echo "1) You need to create .crt file and upload it to mikrotik."
echo "2) Create new identity in mikrotik with copied certificate in remote part"




#apt install strongswan=5.8.2-1ubuntu3.1
