#!/bin/bash
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

DOCKER_VERSION=5:20.10.8~3-0~ubuntu-$(lsb_release -cs)
KUBEADM_VERSION=1.21.3-00
KUBECTL_VERSION=1.21.3-00
KUBELET_VERSION=1.21.3-00
K8S_CP_NODE=k8s-master1

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi


if [ $# -eq 0 ]; then
 printf "Kubernetes token not set.\n You can get it from kubernetes control plane node with \"kubeadm token list\" or generate new \"kubeadm token create\".\n Enter token:"
 read K8S_TOKEN
 printf "Enter k8s cert hash. You can get it run command on control plane node \n""openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | \\ \n openssl rsa -pubin -outform der 2>/dev/null | \\ \n openssl dgst -sha256 -hex | sed \"s/^.* //\" \n Enter cert hash:"
 read K8S_CERT_HASH
fi



#Make sure that the br_netfilter module is loaded. This can be done by running lsmod | grep br_netfilter. 
#To load it explicitly call sudo modprobe br_netfilter.
#As a requirement for your Linux Node's iptables to correctly see bridged traffic, 
#you should ensure net.bridge.bridge-nf-call-iptables is set to 1 in your sysctl config, e.g.

#cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
#br_netfilter
#EOF

#cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
#net.bridge.bridge-nf-call-ip6tables = 1
#net.bridge.bridge-nf-call-iptables = 1
#EOF
#sudo sysctl --system
echo "disable swap"
swapoff -a

#Source https://docs.docker.com/engine/install/ubuntu/


echo "Update the apt package index and install packages to allow apt to use a repository over HTTPS:"
apt-get update
apt-get install apt-transport-https  ca-certificates  curl  gnupg  lsb-release -y
if [ -f /usr/share/keyrings/docker-archive-keyring.gpg] 
then curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
fi

echo "Add Docker’s official GPG key"

echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "Install Docker Engine"
apt-get update
echo $DOCKER_VERSION
apt-get install docker-ce=$DOCKER_VERSION docker-ce-cli=$DOCKER_VERSION containerd.io -y

echo "Check docker installation"
if docker run hello-world 
then echo "Docker instalation success"
else 
 printf "Docker installation check error"
 break
fi


printf "Update the apt package index and install packages needed to use the Kubernetes apt repository:"
#apt-get update
apt-get install -y apt-transport-https ca-certificates curl

printf "Download the Google Cloud public signing key:"
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

printf "Add the Kubernetes apt repository:"

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
printf "Update apt package index, install kubelet, kubeadm and kubectl, and pin their version:"

apt-get update
apt-get install -y kubelet="$KUBELET_VERSION" kubeadm="$KUBEADM_VERSION" kubectl="$KUBECTL_VERSION"
apt-mark hold kubelet kubeadm kubectl

#This packet required for nodes with ceph pod running
printf "install lvm2 packet wich required for ceph"
apt install -y lvm2

kubeadm join --token "$K8S_TOKEN" "$K8S_CP_NODE":6443 --discovery-token-ca-cert-hash sha256:"$K8S_CERT_HASH"

#Install HA cluster https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/
